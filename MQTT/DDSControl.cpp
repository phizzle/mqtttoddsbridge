#include <stdio.h>
#include <stdlib.h>
#include "DDSControl.h"
#include "Utilities.h"
#include "monitor/monitor_common.h"

void payloadListener::on_data_available(DDSDataReader* reader)
{
   payloadDataReader *payload_reader = NULL;
   payloadSeq data_seq;
   DDS_SampleInfoSeq info_seq;
   DDS_ReturnCode_t retcode;
   int i;

   payload_reader = payloadDataReader::narrow(reader);
   if (payload_reader == NULL) {
      Log("DataReader narrow error\n");
      return;
   }

   retcode = payload_reader->take(data_seq, info_seq, DDS_LENGTH_UNLIMITED, DDS_ANY_SAMPLE_STATE, DDS_ANY_VIEW_STATE, DDS_ANY_INSTANCE_STATE);

   if (retcode == DDS_RETCODE_NO_DATA)
      return;

   else if (retcode != DDS_RETCODE_OK) {
      Log("ake error %d\n", retcode);
      return;
   }

   for (i = 0; i < data_seq.length(); ++i) {
      if (info_seq[i].valid_data) {
/*
         g_startTime = GetTimeStamp();

         auto nowClock = std::chrono::steady_clock::now();

         if (g_bInitStartClock) {
            g_startClock = nowClock;
            g_bInitStartClock = false;
         }

         auto timeDelta = std::chrono::duration_cast<std::chrono::milliseconds>(nowClock - g_startClock).count();

         auto iter = g_ddsTopicToDeviceMap.begin();

         if (iter != g_ddsTopicToDeviceMap.end()) {
            for (auto iter1 = iter->second.begin(); iter1 != iter->second.end(); iter1++)
               NotifyDevice(iter1->c_str(), data_seq[i].message, strlen(data_seq[i].message));
         }
*/
      }
   }

   retcode = payload_reader->return_loan(data_seq, info_seq);
   if (retcode != DDS_RETCODE_OK) {
      Log("return loan error %d\n", retcode);
   }
}

DDSTransmitter::DDSTransmitter() 
{
   participant = NULL;
   publisher = NULL;
   subscriber = NULL;
   topic = NULL;
   writer = NULL;
   payload_writer = NULL;
   instance = NULL;
   instance_handle = DDS_HANDLE_NIL;
   sendCount = 0;
}

int DDSTransmitter::publisher_shutdown(DDSDomainParticipant *participant)
{
   DDS_ReturnCode_t retcode;
   int status = 0;

   if (participant != NULL) {
      retcode = participant->delete_contained_entities();
      if (retcode != DDS_RETCODE_OK) {
         printf("delete_contained_entities error %d\n", retcode);
         status = -1;
      }

      retcode = DDSTheParticipantFactory->delete_participant(participant);
      if (retcode != DDS_RETCODE_OK) {
         printf("delete_participant error %d\n", retcode);
         status = -1;
      }
   }

   /* RTI Connext provides finalize_instance() method on
   domain participant factory for people who want to release memory used
   by the participant factory singleton. Uncomment the following block of
   code for clean destruction of the singleton. */
   /*
   retcode = DDSDomainParticipantFactory::finalize_instance();
   if (retcode != DDS_RETCODE_OK) {
   printf("finalize_instance error %d\n", retcode);
   status = -1;
   }
   */
   return status;
}

static int subscriber_shutdown(DDSDomainParticipant *participant)
{
   DDS_ReturnCode_t retcode;
   int status = 0;

   if (participant != NULL) {
      retcode = participant->delete_contained_entities();
      if (retcode != DDS_RETCODE_OK) {
         Log("delete_contained_entities error %d\n", retcode);
         status = -1;
      }

      retcode = DDSTheParticipantFactory->delete_participant(participant);
      if (retcode != DDS_RETCODE_OK) {
         Log("delete_participant error %d\n", retcode);
         status = -1;
      }
   }

   return status;
}

int DDSTransmitter::Initialize(const char *pTopic) 
{
   const char *type_name = NULL;
   int domainId = 0;

   DDS_DomainParticipantQos participant_qos;

   DDS_ReturnCode_t retcode = DDSTheParticipantFactory->get_default_participant_qos(participant_qos);
   if (retcode != DDS_RETCODE_OK) {
      Log("get_participant_qos error %d", retcode);
      return -1;
   }

   retcode = DDSPropertyQosPolicyHelper::add_property(participant_qos.property, "rti.monitor.library", "rtimonitoring", DDS_BOOLEAN_FALSE);
   if (retcode != DDS_RETCODE_OK) {
      Log("add_property error %d", retcode);
      return -1;
   }

   retcode = DDSPropertyQosPolicyHelper::add_pointer_property(participant_qos.property, "rti.monitor.create_function_ptr", (void *)RTIDefaultMonitor_create);
   if (retcode != DDS_RETCODE_OK) {
      Log("add_pointer_property error %d", retcode);
      return -1;
   }

   participant = DDSTheParticipantFactory->create_participant(domainId, participant_qos, NULL /* listener */, DDS_STATUS_MASK_NONE);
   if (participant == NULL) {
      Log("create_participant error\n");
      publisher_shutdown(participant);
      return -1;
   }

   publisher = participant->create_publisher(DDS_PUBLISHER_QOS_DEFAULT, NULL /* listener */, DDS_STATUS_MASK_NONE);
   if (publisher == NULL) {
      printf("create_publisher error\n");
      publisher_shutdown(participant);
      return -1;
   }

   subscriber = participant->create_subscriber(DDS_SUBSCRIBER_QOS_DEFAULT, NULL /* listener */, DDS_STATUS_MASK_NONE);
   if (subscriber == NULL) {
      Log("create_subscriber error\n");
      subscriber_shutdown(participant);
      return -1;
   }

   type_name = payloadTypeSupport::get_type_name();

   retcode = payloadTypeSupport::register_type(participant, type_name);
   if (retcode != DDS_RETCODE_OK) {
      printf("register_type error %d\n", retcode);
      publisher_shutdown(participant);
      return -1;
   }

   topic = participant->create_topic(pTopic, type_name, DDS_TOPIC_QOS_DEFAULT, NULL /* listener */, DDS_STATUS_MASK_NONE);
   if (topic == NULL) {
      printf("create_topic error\n");
      publisher_shutdown(participant);
      return -1;
   }

   writer = publisher->create_datawriter(topic, DDS_DATAWRITER_QOS_DEFAULT, NULL /* listener */, DDS_STATUS_MASK_NONE);
   if (writer == NULL) {
      printf("create_datawriter error\n");
      publisher_shutdown(participant);
      return -1;
   }

   payload_writer = payloadDataWriter::narrow(writer);
   if (payload_writer == NULL) {
      printf("DataWriter narrow error\n");
      publisher_shutdown(participant);
      return -1;
   }

   instance = payloadTypeSupport::create_data();
   if (instance == NULL) {
      printf("payloadTypeSupport::create_data error\n");
      publisher_shutdown(participant);
      return -1;
   }

   reader_listener = new payloadListener();

   DDSDataReader *reader = NULL;

   DDSTopic *pStatus = participant->create_topic("status", type_name, DDS_TOPIC_QOS_DEFAULT, NULL /* listener */, DDS_STATUS_MASK_NONE);
   if (pStatus == NULL) {
      Log("create_topic error\n");
      return -1;
   }

   /* To customize the data reader QoS, use
   the configuration file USER_QOS_PROFILES.xml */
   reader = subscriber->create_datareader(pStatus, DDS_DATAREADER_QOS_DEFAULT, reader_listener, DDS_STATUS_MASK_ALL);
   if (reader == NULL) {
      Log("create_datareader error\n");
      delete reader_listener;
      reader_listener = NULL;
      return -1;
   }

   return 0;
}

int DDSTransmitter::Terminate() 
{
   if (instance) {
      retcode = payloadTypeSupport::delete_data(instance);
      if (retcode != DDS_RETCODE_OK) {
         printf("payloadTypeSupport::delete_data error %d\n", retcode);
      }
   }
   if (participant || subscriber) {
      publisher_shutdown(participant);
   }

   return 0;
}

int DDSTransmitter::Send(const char *pData) 
{
   if (instance) {
      strcpy(instance->message, pData);

      retcode = payload_writer->write(*instance, DDS_HANDLE_NIL);
      if (retcode != DDS_RETCODE_OK) {
         Log("write error %d\n", retcode);
         return retcode;
      }
      sendCount++;

      if ((sendCount % 50) == 0)
         Log("Sent %d", sendCount);
   }

   return 0;
}

DDSTransmitter g_ddsTransmitter;

void StartupDDS()
{
   g_ddsTransmitter.Initialize(TEST_TOPIC);
}

void ShutdownDDS()
{
   g_ddsTransmitter.Terminate();
}

void SendDDS(const char *pData)
{
   g_ddsTransmitter.Send(pData);
}