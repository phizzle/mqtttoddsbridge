#include <mosquitto.h>
#include <string>
#include <thread>
#include <chrono>
#include <mutex>
#include <condition_variable>

#include "string.h"

#include "Utilities.h"

#define DEFAULT_MQTT_HOST   "192.168.1.1"
#define DEFAULT_MQTT_PORT   1883
#define MQTT_TEST_TOPIC     "car_router_3"//"MQTT_Test_Topic"

struct mosquitto *g_pMosq = NULL;

std::string g_mqttHost = DEFAULT_MQTT_HOST;
int g_mqttPort = DEFAULT_MQTT_PORT;

std::thread g_mqttThread;

void MQTTThread()
{
   extern std::mutex g_signalMutex;
   extern bool       g_bWaitForSignal;

   do
   {
      std::unique_lock<std::mutex> lock(g_signalMutex);

      if (!g_bWaitForSignal)
         return;

      lock.unlock();

      int err = mosquitto_loop(g_pMosq, -1, 1);
      if (err != MOSQ_ERR_SUCCESS) {
         Log("MQTTThread reconnect due to %s", mosquitto_strerror(err));
         std::this_thread::sleep_for(std::chrono::milliseconds(100));
         mosquitto_reconnect(g_pMosq);
      }
   } while (1);
}

void ShutdownMQTT()
{
   if (g_pMosq) {

      if (g_mqttThread.joinable())
         g_mqttThread.join();

      int ret = mosquitto_disconnect(g_pMosq);

      if (ret)
         Log("Can't disconnect from Mosquitto borker");
      else
         Log("Disconnected from Mosquitto broker");

      mosquitto_destroy(g_pMosq);

      g_pMosq = NULL;
   }

   mosquitto_lib_cleanup();
}

void mqttCallback(struct mosquitto *mosq, void *obj, const struct mosquitto_message *message)
{
//   Log("Topic: %s, Payload: %s", message->topic, (const char *) message->payload);

   extern void SendDDS(const char *pData);

   //SendDDS("This is a test");

   SendDDS((const char *) message->payload);
}

int MQTTSubscribe(const char *pTopic)
{
   int ret = mosquitto_subscribe(g_pMosq, NULL, pTopic, 0);
   if (ret)
      Log("Can't subscribe to '%s' on Mosquitto server", pTopic);
   else
      Log("Subscribed to '%s' on Mosquitto server", pTopic);
   return ret;
}

int MQTTSend(const char *pTopic, const char *pMessage)
{
   if (!g_pMosq)
      return -1;

   return mosquitto_publish(g_pMosq, NULL, pTopic, strlen(pMessage), pMessage, 0, false);
}

int StartupMQTT()
{
   int ret = 0;

   do {
      ret = mosquitto_lib_init();
      if (ret) {
         Log("Can't initialize Mosquitto library");
         break;
      }
      Log("Initialized Mosquitto library");

      g_pMosq = mosquitto_new(NULL, true, NULL);
      if (!g_pMosq) {
         Log("Can't construct Mosquitto client");
         ret = -1;
         break;
      }
      Log("Constructed Mosquitto client");

      mosquitto_username_pw_set(g_pMosq, NULL, NULL);

      ret = mosquitto_connect(g_pMosq, g_mqttHost.c_str(), g_mqttPort, 10);
      if (ret) {
         Log("Can't connect to Mosquitto server at %s:%d", g_mqttHost.c_str(), g_mqttPort);
         break;
      }
      Log("Connected to Mosquitto server at %s:%d", g_mqttHost.c_str(), g_mqttPort);

      ret = MQTTSubscribe(MQTT_TEST_TOPIC);
      if (ret)
         break;

      mosquitto_message_callback_set(g_pMosq, mqttCallback);

      g_mqttThread = std::thread(MQTTThread);

      Log("Started Mosquitto Thread");

      break;

   } while (1);

   if (ret)
      ShutdownMQTT();

   return ret;
}
