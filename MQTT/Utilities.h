#ifndef UTILITIES_HEADER_INCLUDED
#define UTILITIES_HEADER_INCLUDED

#ifndef LINUX_BUILD
extern char *optarg;
int getopt(int nargc, char * const nargv[], const char *ostr);
#endif

void SetFolder(const char *pFolder);

extern "C" void Log(const char *pData, ...);

#define TEST_TOPIC "car_router_3"

#endif
