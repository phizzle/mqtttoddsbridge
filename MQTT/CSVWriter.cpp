#include <fstream>
#include "CSVWriter.h"

template<typename T>
void CSVWriter::addDataInRow(T first, T last) {

    std::fstream file;
    file.open(fileName, std::ios::out | (linesCount ? std::ios::app : std::ios::trunc));

    while(first != last) {
        file << *first;
        if(++first != last)
            file << delimiter;
    }
    file << "\n";
    linesCount++;

    file.close();
}