#ifndef DDS_Transmitter_Included
#define DDS_Transmitter_Included

#include "ndds/ndds_cpp.h"
#include "ddsPayload.h"
#include "ddsPayloadSupport.h"

class payloadListener : public DDSDataReaderListener {
public:
   virtual void on_requested_deadline_missed(
      DDSDataReader* /*reader*/,
      const DDS_RequestedDeadlineMissedStatus& /*status*/) {}

   virtual void on_requested_incompatible_qos(
      DDSDataReader* /*reader*/,
      const DDS_RequestedIncompatibleQosStatus& /*status*/) {}

   virtual void on_sample_rejected(
      DDSDataReader* /*reader*/,
      const DDS_SampleRejectedStatus& /*status*/) {}

   virtual void on_liveliness_changed(
      DDSDataReader* /*reader*/,
      const DDS_LivelinessChangedStatus& /*status*/) {}

   virtual void on_sample_lost(
      DDSDataReader* /*reader*/,
      const DDS_SampleLostStatus& /*status*/) {}

   virtual void on_subscription_matched(
      DDSDataReader* /*reader*/,
      const DDS_SubscriptionMatchedStatus& /*status*/) {}

   virtual void on_data_available(DDSDataReader* reader);
};

class DDSTransmitter
{
   public    : DDSTransmitter();
           
               int Initialize(const char *pTopic);
               int Terminate();
               int Send(const char *pData);

   protected :
               int publisher_shutdown(DDSDomainParticipant *participant);
        
               DDSDomainParticipant *participant;
               DDSPublisher         *publisher;
               DDSSubscriber        *subscriber;
               DDSTopic             *topic;
               DDSDataWriter        *writer;
               payloadDataWriter    *payload_writer;
               payloadListener      *reader_listener = NULL;
               payload              *instance;
               DDS_InstanceHandle_t instance_handle;
               DDS_ReturnCode_t     retcode;
               int                  sendCount;
};

void StartupDDS();
void ShutdownDDS();
void SendDDS(const char *pData);

#endif