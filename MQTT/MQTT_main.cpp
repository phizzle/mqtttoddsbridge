// MQTT.cpp : Defines the entry point for the console application.
//

#include <signal.h>
#include <thread>
#include <chrono>
#include <mutex>
#include <condition_variable>
#include <iostream>

#ifdef LINUX_BUILD
#include <unistd.h>
#endif

#include "Utilities.h"
#include "MQTT.h"
#include "DDSControl.h"
#include "CSVWriter.h"
#include <vector>

std::mutex              g_signalMutex;
std::condition_variable g_signalCondition;
bool                    g_bWaitForSignal = true;

void sighandler(int sig)
{
   std::lock_guard<std::mutex> lock(g_signalMutex);
   g_bWaitForSignal = false;

   g_signalCondition.notify_one();
}

void InitializeSignals()
{
   signal(SIGABRT, &sighandler);
   signal(SIGTERM, &sighandler);
   signal(SIGINT, &sighandler);
}

void ProcessArgs(int argc, char **argv)
{
   extern std::string g_mqttHost;
   extern int         g_mqttPort;

   int opt;

   while ((opt = getopt(argc, argv, "b:p:f:c:")) != -1) {
      switch (opt) {
      case 'b':
         g_mqttHost = optarg;
         break;
      case 'p':
         g_mqttPort = atoi(optarg);
         break;
      case 'f':
         SetFolder(optarg);
         break;
      }
   }
}

void ProcessEvents()
{
   std::unique_lock<std::mutex> lock(g_signalMutex);
   
   g_signalCondition.wait(lock);
}

std::chrono::time_point<std::chrono::high_resolution_clock> g_startTime;

int main(int argc, char **argv)
{
   Log("Main Entry");

   InitializeSignals();

   ProcessArgs(argc, argv);

   StartupMQTT();

   StartupDDS();

   ProcessEvents();

   ShutdownMQTT();

   ShutdownDDS();

   Log("Main Exit");

   return 0;
}

