#include <string>
#include <time.h>
#include <stdarg.h>
#include <fstream>
#include <sys/stat.h>
#ifdef LINUX_BUILD
#include <unistd.h> 
#else
#include <direct.h>
#endif

#include <mutex>

#define MAX_LOG_FILE_LENGTH (1024 * 1024)

static std::mutex s_logLock;

const std::string CurrentDateTime(bool bLogFormat) {
   time_t     now = time(0);
   struct tm  tstruct;
   char       buf[80];
   tstruct = *localtime(&now);
   if (bLogFormat)
      strftime(buf, sizeof(buf), "%Y-%m-%d %X", &tstruct);
   else
      strftime(buf, sizeof(buf), "%Y%m%d%H%M%S", &tstruct);
   return buf;
}

const char *LogFileName() {
   static std::string logFileName = "";

   if (logFileName.length() == 0) {
      logFileName = CurrentDateTime(false);
      logFileName += ".txt";
   }
   else {
      struct stat sb;

      if ((stat(logFileName.c_str(), &sb) == -1) || (sb.st_size > MAX_LOG_FILE_LENGTH)) {
         logFileName = CurrentDateTime(false);
         logFileName += ".txt";
      }
   }

   return logFileName.c_str();
}

extern "C" void Log(const char *pData, ...)
{
   s_logLock.lock();

   char logData[1024];

   va_list args;
   va_start(args, pData);
   vsprintf(logData, pData, args);
   va_end(args);

   std::ofstream outfile;

   outfile.open(LogFileName(), std::ios_base::app);
   outfile << CurrentDateTime(true).c_str();
   outfile << " : ";
   outfile << logData;
   outfile << std::endl;
   outfile.close();

   fprintf(stderr, "%s\n", logData);

   s_logLock.unlock();
}

#ifndef LINUX_BUILD

int     opterr = 1,             /* if error message should be printed */
optind = 1,             /* index into parent argv vector */
optopt,                 /* character checked for validity */
optreset;               /* reset getopt */
char    *optarg;                /* argument associated with option */

#define BADCH   (int)'?'
#define BADARG  (int)':'
#define EMSG    0

int getopt(int nargc, char * const nargv[], const char *ostr)
{
   char placeHack[1024];

   placeHack[0] = 0;

   char *place = placeHack;

   const char *oli;                        /* option letter list index */

   if (optreset || !*place) {              /* update scanning pointer */
      optreset = 0;
      if (optind >= nargc || *(place = nargv[optind]) != '-') {
         place = EMSG;
         return (-1);
      }
      if (place[1] && *++place == '-') {      /* found "--" */
         ++optind;
         place = EMSG;
         return (-1);
      }
   }                                       /* option letter okay? */
   if ((optopt = (int)*place++) == (int)':' ||
      !(oli = strchr(ostr, optopt))) {
      /*
      * if the user didn't specify '-' as an option,
      * assume it means -1.
      */
      if (optopt == (int)'-')
         return (-1);
      if (!*place)
         ++optind;
      if (opterr && *ostr != ':')
         Log("illegal option -- %c", optopt);
      return (BADCH);
   }
   if (*++oli != ':') {                    /* don't need argument */
      optarg = NULL;
      if (!*place)
         ++optind;
   }
   else {                                  /* need an argument */
      if (*place)                     /* no white space */
         optarg = place;
      else if (nargc <= ++optind) {   /* no arg */
         place = EMSG;
         if (*ostr == ':')
            return (BADARG);
         if (opterr)
            Log("option requires an argument -- %c", optopt);
         return (BADCH);
      }
      else                            /* white space */
         optarg = nargv[optind];
      place = EMSG;
      ++optind;
   }
   return (optopt);                        /* dump back option letter */
}

#endif

void SetFolder(const char *pFolder)
{
#ifdef LINUX_BUILD
   chdir(optarg);
#else
   _chdir(optarg);
#endif
}