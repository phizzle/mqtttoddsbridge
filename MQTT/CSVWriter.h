//
// Created by evan on 7/6/18.
//

#ifndef MQTT_TEST_CSV_H
#define MQTT_TEST_CSV_H

#include <string>
#include <vector>
using namespace std;

class CSVWriter {
public:

    CSVWriter(string fileName, string delimiter = ",") :
            fileName(fileName), delimiter(delimiter), linesCount(0) {};


    template<typename T>
    void addDataInRow(T first, T last);

private:

    string fileName;
    std::string delimiter;
    int linesCount;
};

class CTestClass
{
   CTestClass()
   {
      CSVWriter w("test123.csv");
      std::vector<std::string> dataList_1 = { "20", "hi", "99" };

      w.addDataInRow<std::vector<std::string>::iterator>(dataList_1.begin(), dataList_1.end());
   }
      
};

#endif //MQTT_TEST_CSV_H
