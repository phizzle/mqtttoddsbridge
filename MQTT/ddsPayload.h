

/*
WARNING: THIS FILE IS AUTO-GENERATED. DO NOT MODIFY.

This file was generated from ddsPayload.idl using "rtiddsgen".
The rtiddsgen tool is part of the RTI Connext distribution.
For more information, type 'rtiddsgen -help' at a command shell
or consult the RTI Connext manual.
*/

#ifndef ddsPayload_1949509771_h
#define ddsPayload_1949509771_h

#ifndef NDDS_STANDALONE_TYPE
#ifndef ndds_cpp_h
#include "ndds/ndds_cpp.h"
#endif
#else
#include "ndds_standalone_type.h"
#endif

extern "C" {

    extern const char *payloadTYPENAME;

}

struct payloadSeq;
#ifndef NDDS_STANDALONE_TYPE
class payloadTypeSupport;
class payloadDataWriter;
class payloadDataReader;
#endif

class payload 
{
  public:
    typedef struct payloadSeq Seq;
    #ifndef NDDS_STANDALONE_TYPE
    typedef payloadTypeSupport TypeSupport;
    typedef payloadDataWriter DataWriter;
    typedef payloadDataReader DataReader;
    #endif

    DDS_Char *   message ;

};
#if (defined(RTI_WIN32) || defined (RTI_WINCE)) && defined(NDDS_USER_DLL_EXPORT)
/* If the code is building on Windows, start exporting symbols.
*/
#undef NDDSUSERDllExport
#define NDDSUSERDllExport __declspec(dllexport)
#endif

NDDSUSERDllExport DDS_TypeCode* payload_get_typecode(void); /* Type code */

DDS_SEQUENCE(payloadSeq, payload);

NDDSUSERDllExport
RTIBool payload_initialize(
    payload* self);

NDDSUSERDllExport
RTIBool payload_initialize_ex(
    payload* self,RTIBool allocatePointers,RTIBool allocateMemory);

NDDSUSERDllExport
RTIBool payload_initialize_w_params(
    payload* self,
    const struct DDS_TypeAllocationParams_t * allocParams);  

NDDSUSERDllExport
void payload_finalize(
    payload* self);

NDDSUSERDllExport
void payload_finalize_ex(
    payload* self,RTIBool deletePointers);

NDDSUSERDllExport
void payload_finalize_w_params(
    payload* self,
    const struct DDS_TypeDeallocationParams_t * deallocParams);

NDDSUSERDllExport
void payload_finalize_optional_members(
    payload* self, RTIBool deletePointers);  

NDDSUSERDllExport
RTIBool payload_copy(
    payload* dst,
    const payload* src);

#if (defined(RTI_WIN32) || defined (RTI_WINCE)) && defined(NDDS_USER_DLL_EXPORT)
/* If the code is building on Windows, stop exporting symbols.
*/
#undef NDDSUSERDllExport
#define NDDSUSERDllExport
#endif

#endif /* ddsPayload */

